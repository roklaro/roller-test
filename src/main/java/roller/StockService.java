package roller;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class StockService {

	private final StockManager stockMan;
	private final List<String> stockCommands;
	
	public StockService() {
		this.stockMan = new StockManager();
		this.stockCommands = Arrays.asList(new String[] {"listProducts", "addProduct?name=&lt;productName&gt;", "removeProduct?name=&lt;productName&gt;", "increaseProduct?name=&lt;productName&gt;&amount=&lt;productAmount&gt;", "decreaseProduct?name=&lt;productName&gt;&amount=&lt;productAmount&gt;"});
	}
	
	@RequestMapping("/")
	public String index() {
		return stockCommands.stream().collect(Collectors.joining("<br>"));
	}
	
	@RequestMapping("/listProducts")
	public String listProducts() {
		return this.stockMan.listProducts().stream().map((x) -> String.format("%1$s: %2$d<br>", x.getStockName(), x.getStockAmount())) .collect(Collectors.joining());
	}
	
	@RequestMapping("/addProduct")
	public String addProduct(@RequestParam(value="name") String productName) {
		String returnValue = null;
		StockItem addProduct = stockMan.addProduct(productName);
		if (addProduct == null) {
			returnValue = "product allready exists";
		} else {
			returnValue = "product added to stock";
		}
		return returnValue;
	}
	
	@RequestMapping("/removeProduct")
	public String removeProduct(@RequestParam(value="name") String productName) {
		String returnValue = null;
		StockItem removeProduct = stockMan.removeProduct(productName);
		if (removeProduct == null) {
			returnValue = String.format("product '%1$s' not found in stock", productName);
		} else {
			returnValue = String.format("product '%1$s' with '%2$d' item(s) removed from stock",removeProduct.getStockName(), removeProduct.getStockAmount()); 
		}
		return returnValue;
	}
	
	@RequestMapping("/increaseProduct")
	public String increaseProduct(@RequestParam(value="name") String productName, @RequestParam(value="amount", defaultValue = "1") Integer amount) {
		String returnValue = null;
		StockItem increaseProduct = stockMan.increaseProduct(productName, amount);
		if (increaseProduct == null) {
			returnValue = String.format("product '%1$s' not found in stock", productName);
		} else {
			returnValue = String.format("stock for product '%1$s' is increased from %2$d to %3$d", increaseProduct.getStockName(), increaseProduct.getStockAmount() - amount, increaseProduct.getStockAmount());
		}
		return returnValue;
	}

	@RequestMapping("/decreaseProduct")
	public String decreaseProduct(@RequestParam(value="name") String productName, @RequestParam(value="amount", defaultValue = "1") Integer amount) {
		String returnValue = null;
		StockItem increaseProduct = stockMan.increaseProduct(productName, amount * -1);
		if (increaseProduct == null) {
			returnValue = String.format("product '%1$s' not found in stock", productName);
		} else {
			returnValue = String.format("stock for product '%1$s' is decreased from %2$d to %3$d", increaseProduct.getStockName(), increaseProduct.getStockAmount() + amount, increaseProduct.getStockAmount());
		}
		return returnValue;
	}
}
