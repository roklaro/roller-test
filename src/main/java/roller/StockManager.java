package roller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class StockManager {

	Logger logger = LoggerFactory.getLogger(StockManager.class);
	
	private final Map<String,StockItem> productStockMap;
	
	public StockManager()  {
		this.productStockMap = new HashMap<>();
		this.addProduct("sampleProduct");
		this.addProduct("testProduct");
	}
	
	public List<StockItem> listProducts() {
		logger.info("call listProducts");
		return this.productStockMap.values().stream().collect(Collectors.toList());
	}
	
	public StockItem addProduct(String productName) {
		logger.info(String.format("call addProduct(%1$s)", productName));
		StockItem returnValue = null;
		if (!productStockMap.containsKey(productName)) {
			productStockMap.put(productName, new StockItem(productName));
			returnValue = productStockMap.get(productName);
		} 
		return returnValue;
	}
	
	public StockItem removeProduct(String productName) {
		logger.info(String.format("call removeProduct(%1$s)", productName));
		return productStockMap.remove(productName);
	}
	
	public StockItem increaseProduct(String productName, Integer amount) {
		logger.info(String.format("call increaseProduct(%1$s, %2$d)", productName, amount));
		StockItem returnValue = productStockMap.get(productName);
		if (returnValue != null && returnValue.getStockAmount() + amount >= 0) {
			returnValue.setStockAmount(returnValue.getStockAmount() + amount);
		}
		return returnValue;
	}
}
