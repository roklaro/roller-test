package roller;

public class StockItem {
	
	private final String stockName;
	private Integer stockAmount;
	
	public StockItem(final String stockName) {
		this.stockName = stockName;
		this.stockAmount = 0;
	}

	public Integer getStockAmount() {
		return stockAmount;
	}

	public void setStockAmount(Integer stockAmount) {
		this.stockAmount = stockAmount;
	}

	public String getStockName() {
		return stockName;
	}
	
}
