package roller;

import static org.junit.Assert.*;
import org.junit.Test;

public class StockManagerTest {

	@Test
	public void listProducts() {
		StockManager stockMan = new StockManager();
		assertNotNull(stockMan.listProducts());
	}

	@Test
	public void addProduct() {
		StockManager stockMan = new StockManager();
		StockItem addProduct = stockMan.addProduct("new product");
		assertNotNull(addProduct);
		assertEquals("new product", addProduct.getStockName());
	}

	@Test
	public void removeProduct() {
		StockManager stockMan = new StockManager();
		stockMan.addProduct("new product");
		for (StockItem item : stockMan.listProducts()) {
			stockMan.removeProduct(item.getStockName());
		}
		assertEquals(stockMan.listProducts().size(), 0);
	}

	@Test
	public void increaseProduct() {
		StockManager stockMan = new StockManager();
		StockItem addProduct = stockMan.addProduct("new product");
		Integer oldValue = addProduct.getStockAmount();
		StockItem increaseProduct = stockMan.increaseProduct(addProduct.getStockName(), 101);
		assertTrue(increaseProduct.getStockAmount() == oldValue + 101);
	}
	
	@Test
	public void decreaseProduct() {
		StockManager stockMan = new StockManager();
		StockItem addProduct = stockMan.addProduct("new product");
		Integer oldValue = addProduct.getStockAmount();
		StockItem increaseProduct = stockMan.increaseProduct(addProduct.getStockName(), 101);
		increaseProduct = stockMan.increaseProduct(addProduct.getStockName(), -50);
		assertTrue(increaseProduct.getStockAmount() == oldValue + 101 - 50);		
	}

}
